<?php
/**
 * Project  : tile-expert-test.
 * Author(s): IdbeHoldL
 * Date     : 29.07.2018
 * Time     : 23:28
 */

namespace App\Controllers;

use Silex\Application;
use Silex\Route;
use Silex\Api\ControllerProviderInterface;
use Silex\ControllerCollection;

use App\Repository\ImageRepository;
use App\Repository\UploadHistoryRepository;

class IndexController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        /**
         * @var $uploadHistoryRepository \App\Repository\UploadHistoryRepository
         * @var $imageRepository         \App\Repository\ImageRepository
         */
        $uploadHistoryRepository = $app['repository.upload_history'];
        $imageRepository         = $app['repository.image'];

        $index = new ControllerCollection(new Route());

        /**
         * Main Page
         */
        $index->get('/', function () use ($app, $uploadHistoryRepository) {

            return $app['twig']->render('index/index.twig', [
                'uploadHistory' => $uploadHistoryRepository->getNotEmpty(),
            ]);
        });

        /**
         * Upload history
         */
        $index->get('/history/{id}', function ($id) use ($app, $imageRepository) {

            return $app['twig']->render('index/history.twig', [
                'images' => $imageRepository->findByUploadHistoryId($id),
            ]);
        });

        /**
         * Just page with images
         */
        $index->get('/images', function () use ($app) {

            return $app['twig']->render('index/images.twig', []);
        });

        return $index;
    }
}
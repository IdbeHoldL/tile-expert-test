<?php
/**
 * Project  : tile-expert-test.
 * Author(s): IdbeHoldL
 * Date     : 30.07.2018
 * Time     : 2:48
 */

namespace App\Controllers;


use App\Utils\ImageLoader;
use App\Utils\TileImagePalette;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\Route;

class UploadController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        /* @var $uploadHistoryRepository \App\Repository\ImageRepository */
        $uploadHistoryRepository = $app['repository.upload_history'];
        /* @var $imageRepository \App\Repository\ImageRepository */
        $imageRepository = $app['repository.image'];

        $upload = new ControllerCollection(new Route());

        /**
         * Main Page
         */
        $upload->post('/start', function () use ($app, $uploadHistoryRepository, $imageRepository) {

            $url       = isset($_POST['url']) ? $_POST['url'] : null;
            $minWidth  = isset($_POST['url']) ? $_POST['min_width'] : 200;
            $minHeight = isset($_POST['url']) ? $_POST['min_height'] : 1;

            $uploadId = $uploadHistoryRepository->insert([
                'url'        => $url,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            $imageLoader = new ImageLoader($url, $minWidth, $minHeight);

            $imagesPaths   = [];
            $imagesFound   = 0;
            $imagesSkipped = 0;

            $imageLoader->run(function ($image, $imageHash) use ($uploadId, $imageRepository, &$imagesPaths, &$imagesFound, &$imagesSkipped) {

                $imagesFound++;
                if ($imageRepository->hasImage($imageHash)) {
                    $imagesSkipped++;
                    return;
                }

                // resize image
                $resizeHeight  = 200;
                $imageWidth    = imagesx($image);
                $imageHeight   = imagesy($image);
                $ratio         = $resizeHeight / $imageHeight;
                $newImageWidth = $imageWidth * $ratio;

                $resizedImage = imagecreatetruecolor($newImageWidth, $resizeHeight);
                imagecopyresampled($resizedImage, $image, 0, 0, 0, 0, $newImageWidth, $resizeHeight, $imageWidth, $imageHeight);

                $webImagePath = '/images/' . $imageHash . '.png';
                $imagePath    = WEB_DIR . $webImagePath;

                if (imagepng($resizedImage, $imagePath, 0)) {
                    $palette   = new TileImagePalette($imagePath, 10, 1);
                    $colorName = $palette->getColorName();
                    // add watermark text
                    imagestring($resizedImage, 5, $newImageWidth / 2, $resizeHeight / 2 - 20, $colorName, imagecolorallocate($resizedImage, 255, 0, 0));
                    imagestring($resizedImage, 5, $newImageWidth / 2, $resizeHeight / 2, $colorName, imagecolorallocate($resizedImage, 0, 255, 0));
                    imagestring($resizedImage, 5, $newImageWidth / 2, $resizeHeight / 2 + 20, $colorName, imagecolorallocate($resizedImage, 0, 0, 255));

                    imagepng($resizedImage, $imagePath, 0);

                    $imageRepository->insert([
                        'upload_history_id' => $uploadId,
                        'hash'              => $imageHash,
                        'path'              => $webImagePath,
                        'color'             => TileImagePalette::colorSortOrder($colorName),
                    ]);
                }

                imagedestroy($image);
                imagedestroy($resizedImage);

                $imagesPaths[] = $webImagePath;
            });

            return $app->json([
                'imagesFound'   => $imagesFound,
                'imagesSkipped' => $imagesSkipped,
                'upload'        => $uploadHistoryRepository->findById($uploadId),
                'images'        => $imageRepository->findByUploadHistoryId($uploadId),
            ]);

        });

        return $upload;
    }
}
<?php
/**
 * Project  : tile-expert-test.
 * Author(s): IdbeHoldL
 * Date     : 29.07.2018
 * Time     : 23:30
 */

namespace App\Repository;

use Doctrine\DBAL\Connection;

/**
 * Class Repository
 * @package App\Repository
 */
abstract class AbstractRepository
{
    /**
     * @var Connection
     */
    public $db;

    /**
     * @return string
     */
    abstract public function getTableName();

    /**
     * @param $db Connection
     */
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * @param $fields
     * @return bool|string
     */
    public function insert($fields)
    {
        return ($this->db->insert($this->getTableName(), $fields)) ? $this->db->lastInsertId() : false;
    }

    /**
     * @param array $fields
     * @param array $condition
     * @return int
     */
    public function update(array $fields, array $condition)
    {
        return $this->db->update($this->getTableName(), $fields, $condition);
    }

    /**
     * @param array $condition
     * @return int
     */
    public function delete(array $condition)
    {
        return $this->db->delete($this->getTableName(), $condition);
    }

    /**
     * @param $id
     * @return array|bool
     */
    public function findById($id)
    {
        return $this->db->fetchAssoc(sprintf('SELECT * FROM %s WHERE id = ? LIMIT 1', $this->getTableName()), [(int)$id]);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->db->fetchAll(sprintf('SELECT * FROM %s', $this->getTableName()));
    }
}
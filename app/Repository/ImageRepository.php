<?php
/**
 * Project  : tile-expert-test.
 * Author(s): IdbeHoldL
 * Date     : 29.07.2018
 * Time     : 23:57
 */

namespace App\Repository;

/**
 * Class ImageRepository
 * @package App\Repository
 */
class ImageRepository extends AbstractRepository
{
    /**
     * @return string
     */
    public function getTableName()
    {
        return 'images';
    }

    /**
     * @param $uploadHistoryId
     * @return array
     */
    public function findByUploadHistoryId($uploadHistoryId)
    {
        return $this->db->fetchAll(sprintf('SELECT * FROM %s WHERE upload_history_id = ? ORDER BY color ASC', $this->getTableName()), [(int)$uploadHistoryId]);
    }

    /**
     * @param $hash
     * @return bool
     */
    public function hasImage($hash)
    {
        return (bool)$this->db->fetchColumn(sprintf('SELECT EXISTS (SELECT * FROM %s WHERE hash = :hash)', $this->getTableName()), ['hash' => $hash], 0);
    }
}
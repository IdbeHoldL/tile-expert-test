<?php
/**
 * Project  : tile-expert-test.
 * Author(s): IdbeHoldL
 * Date     : 30.07.2018
 * Time     : 1:51
 */

namespace App\Repository;

/**
 * Class UploadHistoryRepository
 * @package App\Repository
 */
class UploadHistoryRepository extends AbstractRepository
{
    /**
     * @return string
     */
    public function getTableName()
    {
        return 'upload_history';
    }

    /**
     * Get all not empty uploads from history
     * @return array
     */
    public function getNotEmpty()
    {
        return $this->db->fetchAll('SELECT 
                                        upload_history.*, Count(images.id) as count_images 
                                    FROM upload_history 
                                        LEFT JOIN images ON (images.upload_history_id = upload_history.id)  
                                    GROUP BY upload_history.id
                                    HAVING count_images  > 0 
                                    ORDER BY created_at DESC ');
    }
}
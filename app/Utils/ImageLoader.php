<?php
/**
 * Project  : tile-expert-test.
 * Author(s): IdbeHoldL
 * Date     : 30.07.2018
 * Time     : 3:03
 */

namespace App\Utils;

use JMathai\PhpMultiCurl\MultiCurl;

/**
 * Class ImageLoader
 * @package App\Utils\ImageLoader
 */
class ImageLoader
{
    public $resizeWidth = 200;
    /**
     * Site url
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $urlInfo;


    /**
     * Min image width
     * @var int
     */
    private $minWidth;

    /**
     * Min image height
     * @var int
     */
    private $minHeight;

    /**
     * ImageLoader constructor.
     * @param $url
     * @param $minWidth
     * @param $minHeight
     */
    public function __construct($url, $minWidth, $minHeight)
    {
        if (!preg_match('/^http(s|):\/\/?/i', $url)) {
            $url = 'http://' . $url;
        }

        $this->url       = $url;
        $this->urlInfo   = parse_url($url);
        $this->minWidth  = $minWidth;
        $this->minHeight = $minHeight;
    }


    /**
     * Load images with multi-curl
     * @param null $onLoadCallBack
     * @return array
     */
    public function run($onLoadCallBack = null)
    {
        $pageContent = $this->getPageContent();
        $imagesLinks = $this->getPageImages($pageContent);

        $multiCurl = MultiCurl::getInstance();
        $curlCalls = [];
        foreach ($imagesLinks as $imagesLink) {
            $curlHandle = curl_init();
            curl_setopt($curlHandle, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curlHandle, CURLOPT_URL, $imagesLink);
            curl_setopt($curlHandle, CURLOPT_HEADER, false);
            curl_setopt($curlHandle, CURLOPT_TIMEOUT, 10);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_BINARYTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curlHandle, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.1 Safari/537.11');
            $curlCalls[] = $multiCurl->addCurl($curlHandle);
        }

        $images = [];
        foreach ($curlCalls as $curlCall) {
            if ($content = $curlCall->response) {
                if (($image = @imagecreatefromstring($content)) && imagesx($image) >= $this->minWidth && imagesy($image) >= $this->minHeight) {
                    $images[] = $image;

                    if (is_callable($onLoadCallBack)) {
                        $onLoadCallBack($image, md5($content));
                    }
                }
            }
        }

        return $images;
    }

    /**
     * Get url page content
     * @return mixed|string
     */
    private function getPageContent()
    {
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curlHandle, CURLOPT_URL, $this->url);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 10);
        if (!$result = curl_exec($curlHandle)) {
            return '';
        }
        curl_close($curlHandle);

        return $result;
    }

    /**
     * Get page Images
     * Returns absolute images urls
     * @param $pageContent
     * @return array
     */
    public function getPageImages($pageContent)
    {
        $imagesLinks = [];
        $doc         = new \DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($pageContent);

        $xpath    = new \DOMXPath($doc);
        $nodeList = $xpath->query("//img");

        foreach ($nodeList as $node) {
            if ($imageSrc = $node->attributes->getNamedItem('src')->nodeValue) {

                //
                $imagesLinks[] = preg_match('/^http(s|):\/\/?/i', $imageSrc)
                    ? $imageSrc
                    : $this->urlInfo['scheme'] . '://' . $this->urlInfo['host'] . (preg_match('/^[\/]\s?/i', $imageSrc)
                        ? $imageSrc
                        : (isset($this->urlInfo['path'])
                            ? dirname($this->urlInfo['path'])
                            : '') . '/' . $imageSrc);
            }
        }

        return $imagesLinks;
    }
}
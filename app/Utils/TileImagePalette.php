<?php
/**
 * Project  : tile-expert-test.
 * Author(s): IdbeHoldL
 * Date     : 30.07.2018
 * Time     : 8:02
 */

namespace App\Utils;


use BrianMcdo\ImagePalette\ImagePalette;

class TileImagePalette extends ImagePalette
{

    protected $whiteList = [
        0x000000,
        0x65319F, 0x98329E,
        0x346600, 0x77CD00, 0x679A00, 0x9A9900, 0x676600,
        0xFFFB00, 0xF5DE00, 0xFFCD00,
        0xFE6700, 0xFF9900,
        0xCB1C00, 0xCB342D,
        0xffffff,
    ];

    public static $namedColors = [
        'black'  => [0x000000],
        'purple' => [0x98329E],
        'blue'   => [0x0064D3, 0x0098D1, 0x3043D9],
        'green'  => [0x346600, 0x77CD00, 0x679A00, 0x9A9900, 0x676600],
        'yellow' => [0xFFFB00, 0xF5DE00, 0xFFCD00],
        'orange' => [0xFE6700, 0xFF9900],
        'red'    => [0xCB1C00, 0xCB342D],
        'white'  => [0xffffff],
    ];

    /**
     * Get color index
     * @param $colorName
     * @return int
     */
    public static function colorSortOrder($colorName)
    {
        $colors = array_keys(self::$namedColors);
        $index  = array_search($colorName, $colors);

        return ($index !== false) ? $index : 999; // other colors
    }

    /**
     * @return int|string
     */
    public function getColorName()
    {
        if ($color = $this->getColors(1)[0]) {
            foreach (self::$namedColors as $name => $colors) {
                foreach ($colors as $namedColor) {
                    if ((int)$namedColor === $color->toInt()) {
                        return $name;
                    }
                }
            }
        }

        return '';
    }


}
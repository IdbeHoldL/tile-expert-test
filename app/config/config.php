<?php
/**
 * Project  : tile-expert-test.
 * Author(s): IdbeHoldL
 * Date     : 29.07.2018
 * Time     : 23:01
 */

$app['debug'] = true;

$dbOptions = require "database.php";

$app->register(new Silex\Provider\DoctrineServiceProvider(), [
    'db.options' => $dbOptions,
]);

$app->register(new Silex\Provider\TwigServiceProvider(), [
    'twig.path' => __DIR__ . '/../views',
]);

$app['repository.image']          = function ($app) {
    return new \App\Repository\ImageRepository($app['db']);
};
$app['repository.upload_history'] = function ($app) {
    return new \App\Repository\UploadHistoryRepository($app['db']);
};

$app->mount('/', new App\Controllers\IndexController());
$app->mount('/upload', new App\Controllers\UploadController());
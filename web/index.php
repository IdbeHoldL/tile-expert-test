<?php
/**
 * Project  : tile-expert-test.
 * Author(s): IdbeHoldL
 * Date     : 29.07.2018
 * Time     : 22:38
 */

define('WEB_DIR', __DIR__);
define('UPLOADS_DIR', __DIR__ . '/../uploads');

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();

require_once __DIR__ . '/../app/config/config.php';

$app->run();
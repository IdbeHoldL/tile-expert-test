/**
 * Images row class
 * @param border
 * @constructor
 */
ImageSpreaderRow = function (border) {

    /**
     * Prev row
     * @type {ImageSpreaderRow|null}
     */
    this.prevRow = null;

    /**
     * Newt row
     * @type {ImageSpreaderRow|null}
     */
    this.nextRow = null;

    /**
     * Fixed image wrapper border
     */
    this.border = border;

    /**
     * Row images
     * @type {Image[]}
     */
    this.images = [];

    /**
     * Add image to row
     * @param img
     */
    this.addImage = function (img) {
        this.images.push(img);
    };

    /**
     * Get total images width
     * @return {int}
     */
    this.getImagesWidth = function () {
        var totalWidth = 0;
        for (var ind = 0; ind < this.images.length; ind++) {
            totalWidth += this.images[ind].naturalWidth;
        }

        // console.log('totalWidth', totalWidth);
        return totalWidth;
    };

    /**
     * Get total images border
     * @return {int}
     */
    this.getBorderWidth = function () {
        return this.border * 2 * this.images.length;
    };

    /**
     * Get total row width (images width + border width)
     * @return {int}
     */
    this.getTotalWidth = function () {
        return this.getImagesWidth() + this.getBorderWidth();
    };

    /**
     * Get row first image (left)
     * @return {Image|null}
     */
    this.firstImage = function () {
        return (this.images.length) ? this.images[0] : null;
    };

    /**
     * Get row last image (right)
     * @return {Image|null}
     */
    this.lastImage = function () {
        return (this.images.length) ? this.images[this.images.length - 1] : null;
    };

    /**
     * Get wrappers width for each image
     * @return {int[]}
     */
    this.getImageWrappersWidth = function (rowWidth) {

        var totalWidth       = this.getTotalWidth();
        var totalBorderWidth = this.getBorderWidth();
        var imagesWidth      = this.getImagesWidth();

        // calculate row resize scale (full width with )
        var widthScale = (rowWidth < totalWidth) ? rowWidth / totalWidth : 1;

        // We have fixed border. So calculate border width diff to reduce each image wrapper size on this value
        var fullBorder      = this.border * 2;
        var borderWidthDiff = fullBorder - fullBorder * widthScale;


        var imageWrappersWidth = [];

        // exception for non-filed row
        if (rowWidth > (totalWidth + totalBorderWidth)) {

            return this.images.map(function (image) {
                return image.naturalWidth;
            })
        }

        // first: calculate image wrapper width for each image in row
        var widestImageWrapperWidth = 0;
        var widestImageWrapperIndex = 0;
        var totalWrappersWidth      = 0;

        for (var ind = 0; ind < this.images.length; ind++) {
            var image        = this.images[ind];
            var wrapperWidth = Math.ceil((image.naturalWidth * widthScale) - borderWidthDiff);

            totalWrappersWidth += wrapperWidth;
            imageWrappersWidth.push(wrapperWidth);

            // save widest image wrapper index
            if (widestImageWrapperWidth < wrapperWidth) {
                widestImageWrapperWidth = wrapperWidth;
                widestImageWrapperIndex = ind;
            }
        }

        var wrapperWithBordersWidth = (totalWrappersWidth + totalBorderWidth);
        // now fix CEIL loses by correcting widest image width
        imageWrappersWidth[widestImageWrapperIndex] += (rowWidth - wrapperWithBordersWidth);

        return imageWrappersWidth;
    };

    /**
     * Get row images width
     */
    this.generateRowElement = function (rowWidth) {

        var imageWrappersWidth = this.getImageWrappersWidth(rowWidth);

        var rowDiv       = document.createElement('div');
        rowDiv.className = 'image_row';

        for (var ind = 0; ind < this.images.length; ind++) {

            var wrapperWidth = imageWrappersWidth[ind];

            var curImage              = this.images[ind];
            curImage.style.marginLeft = '-' + Math.floor((curImage.naturalWidth - wrapperWidth) / 2) + 'px';

            var wrapperDiv         = document.createElement('div');
            wrapperDiv.className   = 'image_wrapper';
            wrapperDiv.style.width = wrapperWidth;
            wrapperDiv.appendChild(curImage);

            rowDiv.appendChild(wrapperDiv);
        }
        return rowDiv;
    }
};

ImageSpreader = function (imageContainerId) {

    var that = this;

    this.imageContainer = document.getElementById(imageContainerId);

    /**
     * Border size
     * @type {number}
     */
    this.border = 5;

    /**
     * All images
     * @type {Array}
     */
    this.images = [];

    /**
     * Image rows
     * @type {ImageSpreaderRow[]}
     */
    this.rows = [];

    /**
     * Add Image to spreader
     * @param imagePath
     */
    this.addImage = function (imagePath) {

        var img = new Image();
        img.src = imagePath;

        img.onload = function () {
            that.redraw();
        };

        that.images.push(img);
    };

    /**
     * Add image row
     * @param imageRow
     */
    this.addRow = function (imageRow) {
        if (this.rows.length >= 2) {
            this.rows[this.rows.length - 1].nextRow = imageRow;
            imageRow.prevRow                        = this.rows[this.rows.length - 1];
        }

        this.rows.push(imageRow);
    };

    /**
     * Get widest row
     * @return {ImageSpreaderRow|*}
     */
    this.getWidestRow = function () {
        var widestRowWidth      = 0;
        var widestRowWidthIndex = 0;
        for (var indRow = 0; indRow < this.rows.length; indRow++) {
            var rowWidth = this.rows[indRow].getTotalWidth();
            if (widestRowWidth < rowWidth) {
                widestRowWidth      = rowWidth;
                widestRowWidthIndex = indRow;
            }
        }

        return this.rows[widestRowWidthIndex];
    };

    /**
     * Shake rows
     * Spread images into all rows (averaging width of the rows)
     * @param shakeCount
     */
    this.shakeRows = function (shakeCount) {

        var containerWidth = this.imageContainer.offsetWidth;

        for (var shakeNum = 0; shakeNum <= shakeCount; shakeNum++) {

            var widestRow      = this.getWidestRow();
            var widestRowWidth = widestRow.getTotalWidth();

            if (widestRow.prevRow !== null
                && widestRow.prevRow.getTotalWidth() < widestRowWidth
                && widestRowWidth - widestRow.firstImage().naturalWidth - this.border * 2 >= containerWidth) {

                widestRow.prevRow.images.push(widestRow.images.shift());
                widestRowWidth = widestRow.getTotalWidth(); // save new with (cause it was changed)
            }

            if (widestRow.nextRow !== null
                && widestRow.nextRow.getTotalWidth() < widestRowWidth
                && widestRowWidth - widestRow.lastImage().naturalWidth - this.border * 2 >= containerWidth) {

                widestRow.nextRow.images = [widestRow.images.pop()].concat(widestRow.nextRow.images);
            }
        }
    };

    /**
     * Redraw images in container
     * @return {boolean}
     */
    this.redraw = function () {

        var timeStart = performance.now();

        this.rows          = [];
        var containerWidth = this.imageContainer.offsetWidth;
        var currentRow     = new ImageSpreaderRow(this.border);

        for (var imageInd = 0; imageInd < this.images.length; imageInd++) {

            currentRow.addImage(this.images[imageInd]);
            var currentRowTotalWidth = currentRow.getTotalWidth();

            if (containerWidth <= currentRowTotalWidth) {
                this.addRow(currentRow);
                currentRow = new ImageSpreaderRow(this.border);
            }
        }

        // we have non-filled row
        var lastRowImagesCount = currentRow.images.length;
        if (lastRowImagesCount) {
            if (!this.rows.length) {
                this.addRow(currentRow); // when no rows yet - just add it as is
            } else {
                // move all images to previous row and run "shake"
                for (var ind = 0; ind < lastRowImagesCount; ind++) {
                    this.rows[this.rows.length - 1].addImage(currentRow.images[ind]);
                }
                this.shakeRows(this.rows.length * lastRowImagesCount);
            }
        }

        if (this.rows.length) {
            this.imageContainer.innerHTML = '';
            for (var rowInd = 0; rowInd < this.rows.length; rowInd++) {
                this.imageContainer.appendChild(this.rows[rowInd].generateRowElement(containerWidth));
            }
        }

        var timeEnd = performance.now();
        console.log('Image container redraw end. Render time: ', (timeEnd - timeStart) / 1000 + ' sec.');

        return true;
    }

};


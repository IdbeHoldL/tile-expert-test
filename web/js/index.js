$(function () {

    $('#upload_form').on('submit', function () {

        $('.loader').show();
        $('.info_table_wrapper').hide();

        $.ajax({
            url     : '/upload/start',
            type    : 'POST',
            data    : {
                'url'       : $('#formInputUrl').val(),
                'min_width' : $('#formInputMinWidth').val(),
                'min_height': $('#formInputMinHeight').val()
            },
            dataType: 'json',
            success : function (data) {
                if (typeof data.upload !== 'undefined') {

                    var loadedImages = data.imagesFound - data.imagesSkipped;
                    if (loadedImages) {
                        $('.history_list').prepend(
                            $('<li>').addClass('list-group-item')
                                .append($('<sp  an>').addClass('label label-info upload_date_label').text(data.upload.created_at))
                                .append($('<a>').attr('href', '/history/' + data.upload.id).text(data.upload.url))
                        );
                    }
                    $('#last_request_time').html(data.upload.created_at);
                    $('#last_request_found').html(data.imagesFound);
                    $('#last_request_duplicates').html(data.imagesSkipped);
                    $('#last_request_uploaded').html(loadedImages);
                }

                $('.loader').hide();
                $('.info_table_wrapper').show();

                window.Spreader.images = [];
                for (var ind = 0; ind < data.images.length; ind++) {
                    window.Spreader.addImage(data.images[ind].path)
                }

                window.Spreader.redraw();

                $(':input[type=text]', '#upload_form').val('');
            }
        });

        return false;
    });

});


